//
//  Session.swift
//  TestReview
//
//  Created by Khrystyna Shevchuk on 5/13/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import Foundation

class Session { 
    
    static let shared = Session()
    
    var noteArray = [Note]()
}