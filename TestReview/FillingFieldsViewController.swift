//
//  FillingFieldsViewController.swift
//  TestReview
//
//  Created by Khrystyna Shevchuk on 5/13/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import UIKit

class FillingFieldsViewController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var imageImageView: UIImageView!
    @IBOutlet weak var dateButton: UIButton!
    
    let imagePicker = UIImagePickerController()
    
    var note: Note?
    var date: NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleTextField.text = note?.title
        noteTextView.text = note?.description
        imageImageView.image = note?.image
        setDate(note?.date ?? NSDate())
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
    }
    
    @IBAction func dateButton(sender: AnyObject) {
        performSegueWithIdentifier("datePickerSegue", sender: nil)
    }
    
    @IBAction func saveAction(sender: UIBarButtonItem) {
        if let strongNote = note {
            strongNote.title = titleTextField.text ?? ""
            strongNote.description = noteTextView.text ?? ""
            strongNote.image = imageImageView.image ?? UIImage(named: "image")!
            strongNote.date = date ?? NSDate()
        } else {
            note = Note(title: titleTextField.text ?? "",
                        description: noteTextView.text ?? "",
                        image: imageImageView.image ?? UIImage(named: "image")!,
                        date: self.date ?? NSDate()
            )
            Session.shared.noteArray.append(note!)
            scheduleLocal(note!.title, date: note!.date)
        }
        navigationController?.popViewControllerAnimated(true)
    }
    
    func scheduleLocal(title: String, date: NSDate) {
        let notification = UILocalNotification()
        notification.fireDate = date
        notification.alertBody = title
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
    @IBAction func addImageButton(sender: UIButton) {
        let actionSheet = UIAlertController(title: "Choose photo", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .Default, handler: { action in
            self.pickPhotoWithSourceType(.PhotoLibrary)
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { action in
            self.pickPhotoWithSourceType(.Camera)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    private func pickPhotoWithSourceType(sourceType: UIImagePickerControllerSourceType) {
        if (UIImagePickerController.isSourceTypeAvailable(sourceType)) {
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.sourceType = sourceType
            if sourceType == .Camera {
                imagePicker.cameraCaptureMode = .Photo
            }
            presentViewController(imagePicker, animated: true, completion: nil)
        } else {
            let title = "\(sourceType == .Camera ? "Camera" : "PhotosLibrary") inaccessable"
            let message = "Application cannot access the \(sourceType == .Camera ? "Camera" : "PhotosLibrary")."
            let okButton = "OK"
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: okButton, style: UIAlertActionStyle.Cancel, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? DatePickerViewController {
            vc.completion = { date in
                self.date = date
                self.setDate(date)
            }
        }
    }
    
    private func setDate(date: NSDate) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        self.dateButton.setTitle(dateFormatter.stringFromDate(date), forState: .Normal)
    }
}

extension FillingFieldsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageImageView.contentMode = .ScaleAspectFit
            imageImageView.image = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}












