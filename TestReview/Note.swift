//
//  Note.swift
//  TestReview
//
//  Created by Khrystyna Shevchuk on 5/13/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import UIKit

class Note {
    var title: String
    var description: String
    var image: UIImage
    var date: NSDate
    
    init(title: String, description: String, image: UIImage, date: NSDate) {
        self.title = title
        self.description = description
        self.image = image
        self.date = date
    }
}