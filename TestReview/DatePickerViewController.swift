//
//  DatePickerViewController.swift
//  TestReview
//
//  Created by Khrystyna Shevchuk on 5/16/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    
    var note: Note?
    var completion: ((date: NSDate) -> Void)?
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.addTarget(self, action: #selector(datePickerChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    @IBAction func saveButton(sender: AnyObject) {
        
        completion?(date: datePicker.date)
        
        navigationController?.popViewControllerAnimated(true)
    }
    
    func datePickerChanged(datePicker:UIDatePicker) {
        print(datePicker.date)
//        let dateFormatter = NSDateFormatter()
//        
//        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
//        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
//        note?.date = datePicker.date
    }
}
