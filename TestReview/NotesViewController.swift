//
//  ViewController.swift
//  TestReview
//
//  Created by Khrystyna Shevchuk on 5/13/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var selectedNote: Note?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    
    @IBAction func createNoteButton(sender: UIBarButtonItem) {
        selectedNote = nil //detect that we wanna to add new note
        performSegueWithIdentifier("segue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segue" {
            if let vc = segue.destinationViewController as? FillingFieldsViewController,
                let strongSelectedNote = selectedNote { //check for style: add / edit
                vc.note = strongSelectedNote
            }
        }
    }
}

extension NotesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Session.shared.noteArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! Cell
        
        let note = Session.shared.noteArray[indexPath.row]
        
        cell.imageIcon.image =  note.image
        cell.titleNoteLabel.text = note.title
        cell.descriptionNoteLabel.text = note.description
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedNote = Session.shared.noteArray[indexPath.row]
        performSegueWithIdentifier("segue", sender: nil)
    }
}

